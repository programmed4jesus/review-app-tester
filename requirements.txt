certifi==2019.6.16
chardet==3.0.4
Click==7.0
htmlmin==0.1.12
idna==2.8
Jinja2==2.10.1
jsmin==2.2.2
livereload==2.6.1
Markdown==3.1.1
MarkupSafe==1.1.1
mkdocs==1.0.4
mkdocs-material==4.4.0
mkdocs-minify-plugin==0.2.1
Pygments==2.4.2
pymdown-extensions==6.0
python-gitlab==1.10.0
PyYAML==5.1.2
requests==2.22.0
six==1.12.0
tornado==6.0.3
urllib3==1.25.3
